import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
from pathlib import Path
import tensorflow as tf
from PIL import Image
import numpy as np
import cv2

tf.get_logger().setLevel('ERROR')

gpus = tf.config.experimental.list_physical_devices('GPU')
for gpu in gpus:
    tf.config.experimental.set_memory_growth(gpu, True)

IMAGE_PATHS = Path(
        '/media/thk/transcend_1T/DataSet/pigs/records-20200810/datasets/pig_04/'
        ).glob('*cam2.jpeg')
MODEL_PATH = '/media/thk/transcend_1T/DataSet/pigs/records-20200723/trained/20200830/pig_ssd_inception_v2_200k_frozen/saved_model/'
MODEL_PATH = '/media/thk/transcend_1T/DataSet/pigs/records-20200723/trained/20200902/pig_ssd_inception_v2_200k_frozen/saved_model/'
MODEL_PATH = '/media/thk/transcend_1T/DataSet/pigs/records-20200723/trained/20200903/pig_faster_rcnn_resnet50_200k_frozen/saved_model/'
PATH_TO_LABELS = '/media/thk/transcend_1T/DataSet/pigs/records-20200723/train_eval/pig_label_map.pbtxt'
NUM_CLASSES = 1

#IMAGE_PATHS = Path(
#        '/media/thk/transcend_2T/DataSet/AquaData/grouper/grouper_images/keep_set/'
#        ).glob('*.jpg')
##MODEL_PATH = '/media/thk/transcend_2T/DataSet/AquaData/grouper/training/trained/20200903/grouper_ssd_inception_v2_200k_frozen/saved_model/'
#MODEL_PATH = '/media/thk/transcend_2T/DataSet/AquaData/grouper/training/trained/20200904/grouper_faster_rcnn_resnet50_200k_frozen/saved_model/'
#PATH_TO_LABELS = '/media/thk/transcend_2T/DataSet/AquaData/grouper/training/tf_train_data/20200902/grouper_label_map.pbtxt'
#NUM_CLASSES = 2

TEST_VID = [
        '/media/thk/transcend_1T/DataSet/AquaData/clownfish/vid/STB_盛天合/NGfish_02.mp4',
        '/media/thk/transcend_1T/DataSet/AquaData/clownfish/vid/0929_Side/rawR0929062615.mp4',
        '/media/thk/transcend_1T/DataSet/AquaData/clownfish/vid/AAUUcj9JeME.mkv',
        '/media/thk/transcend_1T/DataSet/AquaData/clownfish/vid/40vzdFKbH_A.mkv',
        '/media/thk/transcend_1T/DataSet/AquaData/clownfish/vid/n1l3dw-Obm0.mp4',
        '/media/thk/transcend_2T/DataSet/AquaData/tilapia/vid/002.mkv',
        '/media/thk/transcend_2T/DataSet/AquaData/tilapia/vid/003.mp4',
        ]
#MODEL_PATH = '/media/thk/transcend_1T/DataSet/AquaData/clownfish/training/trained/20201011_ssd_iv2-aug_rot_vf/repo_12/clownfish_ssd_iv2-aug_rot_vf_repo_12-32k_frozen/saved_model/'
#MODEL_PATH = '/media/thk/transcend_1T/DataSet/AquaData/clownfish/training/trained/20201013_frcnn-aug_rot_vf/repo_01/clownfish_frcnn-aug_rot_vf-42k_frozen/saved_model/'
#MODEL_PATH = '/media/thk/transcend_1T/DataSet/AquaData/clownfish/training/trained/20201013_frcnn-aug_rot_vf/repo_02/clownfish_frcnn-aug_rot_vf-28k_frozen/saved_model/'
#MODEL_PATH = '/media/thk/transcend_1T/DataSet/AquaData/clownfish/training/trained/20201013_frcnn-aug_rot_vf/clownfish_frcnn-aug_rot_vf-102k_frozen/saved_model/'
#PATH_TO_LABELS = '/media/thk/transcend_1T/DataSet/AquaData/clownfish/training/tf_train_data/20201010-aug_rot_vf/clownfish_label_map.pbtxt'
#NUM_CLASSES = 2

MODEL_PATH = '/media/thk/transcend_2T/DataSet/AquaData/tilapia/training/trained/20201016_frcnn/repo_01/tilapia_frcnn-82k_frozen/saved_model/'
PATH_TO_LABELS = '/media/thk/transcend_2T/DataSet/AquaData/tilapia/training/tf_train_data/tilapia_label_map.pbtxt'
NUM_CLASSES = 1

def do_infer(frame, sess):
    h,w,_ = frame.shape
    frame_expanded = np.expand_dims(frame, axis=0)

    image_tensor = 'image_tensor:0'
    boxes = 'detection_boxes:0'
    scores = 'detection_scores:0'
    classes = 'detection_classes:0'
    num_detections = 'num_detections:0'
    # Actual detection.
    boxes, scores, classes, num_detections = sess.run(
            [boxes, scores, classes, num_detections],
            feed_dict={image_tensor: frame_expanded})
    for i in range(20):
        box = boxes[0][i]
        score = scores[0][i]
        class_id = classes[0][i]
        if score > 0.9:
            score_label = f'{score:.2f}'
            y1,x1,y2,x2 = box
            x1 = int(x1*w)
            y1 = int(y1*h)
            x2 = int(x2*w)
            y2 = int(y2*h)
            if class_id == 1:
                cv2.rectangle(frame, (x1,y1), (x2,y2), (255,255,0), 2)
            else:
                cv2.rectangle(frame, (x1,y1), (x2,y2), (0,255,255), 2)
            cv2.putText(frame, str(score_label), (x1,y1), 1, 2, (255,255,255), 2)
    return frame

def main():
    with tf.compat.v1.Session(graph=tf.Graph()) as sess:
        tf.saved_model.loader.load(
                sess, [tf.saved_model.tag_constants.SERVING], MODEL_PATH) 
        cv2.namedWindow('res', cv2.WINDOW_NORMAL)
    
        #for image_f in IMAGE_PATHS:
        #    frame = cv2.imread(str(image_f), 1)
    
        cap = cv2.VideoCapture(TEST_VID[-1])
        while True:
            _, frame = cap.read()
            res_frame = do_infer(frame, sess)
            cv2.imshow('res', res_frame)
            if cv2.waitKey(30) & 0xff == ord('q'):
                break

        cv2.destroyAllWindows()


if __name__=='__main__':
    main()
