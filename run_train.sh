#MODEL_DIR="./trained/20200208/"
#PIPELINE_CONFIG_PATH=${MODEL_DIR}"ssd_mobilenet_v2_coco.config" #new
#NUM_TRAIN_STEPS=200000
#SAMPLE_1_OF_N_EVAL_EXAMPLES=1
#python /media/thk/workspace/github_proj/models/research/object_detection/model_main.py \
#    --pipeline_config_path=${PIPELINE_CONFIG_PATH} \
#    --model_dir=${MODEL_DIR} \
#    --num_train_steps=${NUM_TRAIN_STEPS} \
#    --sample_1_of_n_eval_examples=$SAMPLE_1_OF_N_EVAL_EXAMPLES \
#    --alsologtostderr

##PIPELINE_CONFIG_PATH=${MODEL_DIR}"ssd_inception_v2_coco.config"
#MODEL_DIR="./trained/20200424/"
#PIPELINE_CONFIG_PATH=${MODEL_DIR}"ssd_mobilenet_v2_coco.config"
#NUM_TRAIN_STEPS=200000
#SAMPLE_1_OF_N_EVAL_EXAMPLES=1
#python /media/thk/workspace/github_proj/models/research/object_detection/model_main.py \
#    --pipeline_config_path=${PIPELINE_CONFIG_PATH} \
#    --model_dir=${MODEL_DIR} \
#    --num_train_steps=${NUM_TRAIN_STEPS} \
#    --sample_1_of_n_eval_examples=$SAMPLE_1_OF_N_EVAL_EXAMPLES \
#    --alsologtostderr

#PIPELINE_CONFIG_PATH=${MODEL_DIR}"ssd_inception_v2_coco.config"
##PIPELINE_CONFIG_PATH=${MODEL_DIR}"ssd_mobilenet_v2_coco.config"

#MODEL_DIR="/media/thk/transcend_1T/DataSet/pigs/records-20200723/trained/20200830/"
#MODEL_DIR="/media/thk/transcend_1T/DataSet/pigs/records-20200723/trained/20200902/"
#MODEL_DIR="/media/thk/transcend_1T/DataSet/pigs/records-20200723/trained/20200903/"

#MODEL_DIR="./trained/20200902/" #grouper new
#MODEL_DIR="./trained/20200903/" #grouper new, object number 250, total 500
#PIPELINE_CONFIG_PATH=${MODEL_DIR}"ssd_inception_v2_coco.config"

#MODEL_DIR="/media/thk/transcend_2T/DataSet/AquaData/grouper/training/trained/20200907/"
#MODEL_DIR="/media/thk/transcend_1T/DataSet/pigs/records-20200723/trained/20200913/"
#MODEL_DIR="/media/thk/transcend_2T/DataSet/AquaData/clownfish/training/trained/20201009/"
#PIPELINE_CONFIG_PATH=${MODEL_DIR}"faster_rcnn_resnet50_coco.config"

#MODEL_DIR="/media/thk/transcend_1T/DataSet/AquaData/clownfish/training/trained/20201010_ssd_iv2/" #use smaller initial_learning_rate
#MODEL_DIR="/media/thk/transcend_1T/DataSet/AquaData/clownfish/training/trained/20201010_ssd_iv2_a/" #use random_horizontal_flip
#MODEL_DIR="/media/thk/transcend_1T/DataSet/AquaData/clownfish/training/trained/20201010_ssd_iv2_b/" #use higher classification_weight
#MODEL_DIR="/media/thk/transcend_1T/DataSet/AquaData/clownfish/training/trained/20201010_ssd_iv2-aug_rot/" #use smaller initial_learning_rate with augmented data (rotation)
#MODEL_DIR="/media/thk/transcend_1T/DataSet/AquaData/clownfish/training/trained/20201011_ssd_iv2-aug_rot_vf/" #use default parameters with augmented data (rotation+vflip)
#MODEL_DIR="/media/thk/transcend_1T/DataSet/AquaData/clownfish/training/trained/20201013_frcnn-aug_rot_vf/" #use default parameters with augmented data (rotation+vflip)
MODEL_DIR="/media/thk/transcend_1T/DataSet/AquaData/clownfish/training/trained/20201017_frcnn-aug_rot_vf/"
#PIPELINE_CONFIG_PATH=${MODEL_DIR}"ssd_inception_v2_coco.config"
#MODEL_DIR="/media/thk/transcend_1T/DataSet/AquaData/tilapia/training/trained/20201016_frcnn/"
PIPELINE_CONFIG_PATH=${MODEL_DIR}"faster_rcnn_resnet50_coco.config"
NUM_TRAIN_STEPS=102000
SAMPLE_1_OF_N_EVAL_EXAMPLES=1
python /media/thk/workspace/github_proj/models/research/object_detection/model_main.py \
    --pipeline_config_path=${PIPELINE_CONFIG_PATH} \
    --model_dir=${MODEL_DIR} \
    --num_train_steps=${NUM_TRAIN_STEPS} \
    --sample_1_of_n_eval_examples=$SAMPLE_1_OF_N_EVAL_EXAMPLES \
    --alsologtostderr
