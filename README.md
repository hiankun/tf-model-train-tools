## Add Background Images into TFRecords

* There're many false positive...
* To add images contain no bounding boxes:
  https://github.com/tensorflow/models/issues/3365#issuecomment-661326737

## Preprocessing
* Working folders and file are in the `tuna_detect_proj`:
  * `annos`
  * `images`: Includes all the images from `repo/{albacore, bluefin, yellowfin}`
  * `labels`
* `repo`: Some test as well cannot-be-used-now images
* `tools`
  * `shrink_img.py`: Shrink large images.
    e.g, `python shrink_img.py -f /media/thk/workspace/DataSet/clownfish/`
  * `mv_filename.sh`: Rename files to sequential names.
    Remember to change the filename in the script before using it.
* Use `mogrify -format jpeg *.*` to convert all images to JPEG format.
* Find similar images:
* Use `/media/thk/workspace/gitlab_proj/simple-label/utils/simple_aug.py` to generate
  augmented dataset. (original: `tuna_detect_proj/`; augmented: `tuna_detect_proj-aug/`)


######
* Back up the original folder to `../../tuna_detect_proj-bak.tar.bz2`.
* Combine augmented images and annotations into the `../images/` and `../annos/` folders.
  * NOTE: No need to copy augmented images because the annotation files include the path.
* Create new train folder `trained/20200412`.
  * `cp trained/20200407/ssd_inception_v2_coco.config trained/20200412/`
  * Edit `ssd_inception_v2_coco.config`:
    * `decay_steps` from 5000 to 50000.
    * `num_steps` from 200000 to 300000.

---

[2020-04-07]

In `tf_1_14_env`.

Working folders:
* Data stored at: `/media/thk/workspace/DataSet/tuna/tuna_detect_proj/train_eval`
* [?] Tools for preparing TFRecords: `/media/thk/workspace/gitlab_proj/tf_model_train/`
* Tools for training: `/media/thk/workspace/github_proj/models/research`


# Prepare TFRecords

## Split annotations into train and test files

* First, `cat` all annotation files into one (e.g. `tuna.csv`).
  * Remember to add the following line at the beginning:
    ```
    filename,width,height,class,xmin,ymin,xmax,ymax
    ```
* Edit and run `tf_model_train/tools/split_labels.py`:
```
(data_env) thk tools $ python split_labels.py 
Grouping filenames...
Getting indexes...
Creating lists...
Train items: 556
Test items: 57
/media/thk/workspace/DataSet/tuna/tuna_detect_proj/train_eval/tuna_train.csv
and
/media/thk/workspace/DataSet/tuna/tuna_detect_proj/train_eval/tuna_test.csv
has been saved...
```
* Edit and run `tf_model_train/tools/generate_tfrecord.sh` to generate TFRecords.

# Prepare training files

* Copy `labels` and edit it to `train_eval/tuna_label_map.pbtxt`.
* Copy `pipeline.config` and edit it to `trained/20200407/ssd_inception_v2_coco.config`.
* Edit `run_train.sh` and get the following error:
```
ValueError: SSD Inception V2 feature extractor always usesscope returned by `conv_hyperparams_fn` for both the base feature extractor and the additional layers added since there is no arg_scope defined for the base feature extractor.
```
  * Solution: add `override_base_feature_extractor_hyperparams: true` after `conv_hyperparams{}` in `feature_extractor{}`.
  * ref: [tensorflow/models/issues/7411](https://github.com/tensorflow/models/issues/7411)
---

[2020-07-24]

# Train on Grouper Dataset

* Working folder: `/media/thk/transcend_1T/DataSet/AquaData/grouper/training/20200724`
* Pre-trained model folder: `/media/thk/workspace/gitlab_proj/tf-model-train-tools/models/`
* Trained model folder: `/media/thk/workspace/gitlab_proj/tf-model-train-tools/trained/`

## Prepare TFRecords
* Use `./tools/cat_csv.sh` to combine all the annotations to single csv file:
  `/media/thk/transcend_1T/DataSet/AquaData/grouper/training/20200724/grouper.csv`
* `(tf_1_14_env)`:  Edit and run `./tools/split_labels.py`.
  * Move the output files into `/media/thk/transcend_1T/DataSet/AquaData/grouper/training/20200724/train_eval/`.
* `(tf_1_14_env)`:  Edit and run `./tools/generate_tfrecord.sh`.
  * Edit `~/.bashrc` to modify the following line (I have changed the path of `tf_models`):
    ```
    export PYTHONPATH=$PYTHONPATH:/media/thk/workspace/tf_models/research:/media/thk/workspace/tf_models/research/slim
    ```
* `(tf_1_14_env)`:  Edit and run `./tools/checkers/validate_tfrecords.py`.

## Prepare Pre-trained Models and Config files
* Prepare SSD pre-trained model in Trained model folder (SSD Inception V2)
* Edit `ssd_inception_v2_coco.config`.
* Edit `lables` to `train_eval/grouper_label_map.pbtxt`.

## Run Training
* Edit and run `run_train.sh`.
* __NOTE__: There are errors about
  ```
  tensorflow.python.framework.errors_impl.DataLossError: file is too short to be an sstable
  ```
  It turns out that something corrupted in the pre-trained model.
  Just redownload it from TF Model Zoo and the training went without problems.
