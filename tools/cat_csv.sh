# bash cat_csv.sh input/folder/ <output_path/output.csv>

in_folder="$1" #prevent spaces in the path
out_folder="$(dirname "$1")" #prevent spaces in the path
#output_csv=$out_folder/$2
output_csv=$2

if [ -z "$2" ]
then
    output_csv=$out_folder/output.csv
    echo "No output file supplied. Will output to $output_csv"
fi

if test -f $output_csv; then
    echo "$output_csv exists."
    output_csv="${output_csv%.*}_$(date +%s).csv"
    echo "Output to $output_csv instead."
fi

echo -e "filename,width,height,class,xmin,ymin,xmax,ymax" >> $output_csv
for f in $in_folder/*.csv
do
    cat $f >> $output_csv
done

echo -e "Output file: $output_csv"
