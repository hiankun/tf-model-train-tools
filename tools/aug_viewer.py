import cv2
import pandas as pd

'''
Check the augmented results.
'''

def draw_data(filename, data):
  img = cv2.imread(filename, cv2.IMREAD_UNCHANGED)
  for _, d in data.iterrows():
    _filename, _width, _height, label, xmin , ymin , xmax , ymax = d
    cv2.rectangle(img, (xmin,ymin), (xmax,ymax), (255,0,0), 2)
    cv2.rectangle(img, (xmin,ymin), (xmin+80,ymin+20), (255,0,0), -1)
    cv2.putText(img, label, (xmin+5,ymin+15), 2, 0.5, (255,255,255), 1)

  cv2.imshow('aug results', img)
  cv2.waitKey()


def main():
  aug_file = '../fall_aug.csv'
  df = pd.read_csv(aug_file).groupby(['filename'])
  
  for filename, data in df:
    draw_data(filename, data)

  cv2.destroyAllWindows()


if __name__=='__main__':
  main()
