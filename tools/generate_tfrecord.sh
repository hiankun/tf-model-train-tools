##proj_path="/media/thk/transcend_1T/DataSet/pigs/records-20200723/train_eval/20200902"
##label_path="/media/thk/transcend_1T/DataSet/pigs/records-20200723/label_proj_pig01-03" #nipple
#proj_path="/media/thk/transcend_1T/DataSet/pigs/records-20200723/tf_train_data/20200913"
#label_path="/media/thk/transcend_1T/DataSet/pigs/records-20200723/label_proj_belly" #nipple and belly
#echo "generating pig_test.record..."
#python generate_tfrecord.py \
#    --csv_input ${proj_path}/pig_test.csv \
#    --output_path ${proj_path}/pig_test.record \
#    --labels_file ${label_path}/labels
#
#echo "generating pig_train.record..."
#python generate_tfrecord.py \
#    --csv_input ${proj_path}/pig_train.csv \
#    --output_path ${proj_path}/pig_train.record \
#    --labels_file ${label_path}/labels

##proj_path="/media/thk/transcend_2T/DataSet/AquaData/clownfish/training/tf_train_data/20201009"
##proj_path="/media/thk/transcend_2T/DataSet/AquaData/clownfish/training/tf_train_data/20201010-aug_rot"
#proj_path="/media/thk/transcend_2T/DataSet/AquaData/clownfish/training/tf_train_data/20201010-aug_rot_vf"
#label_path="/media/thk/transcend_2T/DataSet/AquaData/clownfish/training/clownfish_detect_proj_0913"
#echo "generating clownfish_test.record..."
#python generate_tfrecord.py \
#    --csv_input ${proj_path}/clownfish_test.csv \
#    --output_path ${proj_path}/clownfish_test.record \
#    --labels_file ${label_path}/labels
#
#echo "generating clownfish_train.record..."
#python generate_tfrecord.py \
#    --csv_input ${proj_path}/clownfish_train.csv \
#    --output_path ${proj_path}/clownfish_train.record \
#    --labels_file ${label_path}/labels

proj_path="/media/thk/transcend_1T/DataSet/AquaData/tilapia/training/tf_train_data"
label_path="/media/thk/transcend_1T/DataSet/AquaData/tilapia/training/tilapia_detect_proj_1012"
echo "generating tilapia_test.record..."
python generate_tfrecord.py \
    --csv_input ${proj_path}/tilapia_test.csv \
    --output_path ${proj_path}/tilapia_test.record \
    --labels_file ${label_path}/labels

echo "generating tilapia_train.record..."
python generate_tfrecord.py \
    --csv_input ${proj_path}/tilapia_train.csv \
    --output_path ${proj_path}/tilapia_train.record \
    --labels_file ${label_path}/labels

##proj_path="/media/thk/transcend_1T/DataSet/AquaData/grouper/training/20200724/"
#proj_path="/media/thk/transcend_2T/DataSet/AquaData/grouper/training/tf_train_data/20200902"
#label_path="/media/thk/transcend_2T/DataSet/AquaData/grouper/training/label_proj_0902"
#echo "generating grouper_test.record..."
#python generate_tfrecord.py \
#    --csv_input ${proj_path}/grouper_test.csv \
#    --output_path ${proj_path}/grouper_test.record \
#    --labels_file ${label_path}/labels
#
#echo "generating grouper_train.record..."
#python generate_tfrecord.py \
#    --csv_input ${proj_path}/grouper_train.csv \
#    --output_path ${proj_path}/grouper_train.record \
#    --labels_file ${label_path}/labels


#===================================================================================
#echo "generating twice_train.record..."
#python generate_tfrecord.py \
#    --csv_input train_labels.csv \
#    --output_path twice_train.record \
#    --image_dir ../twice_pics/ \
#    --labels_file ../twice_data/twice_members.labels
#
#echo "generating twice_test.record..."
#python generate_tfrecord.py \
#    --csv_input test_labels.csv \
#    --output_path twice_test.record \
#    --image_dir ../twice_pics/ \
#    --labels_file ../twice_data/twice_members.labels

#echo "generating fall_aug_test.record..."
#python generate_tfrecord.py \
#    --csv_input /media/thk/workspace/DataSet/DetAction2019/fall_test.csv \
#    --output_path /media/thk/workspace/DataSet/DetAction2019/fall_test.record \
#    --image_dir /media/thk/workspace/DataSet/DetAction2019/fall_jpegs/ \
#    --labels_file /media/thk/workspace/DataSet/DetAction2019/labels
#
#echo "generating fall_aug_train.record..."
#python generate_tfrecord.py \
#    --csv_input /media/thk/workspace/DataSet/DetAction2019/fall_train.csv \
#    --output_path /media/thk/workspace/DataSet/DetAction2019/fall_train.record \
#    --image_dir /media/thk/workspace/DataSet/DetAction2019/fall_jpegs/ \
#    --labels_file /media/thk/workspace/DataSet/DetAction2019/labels

#echo "generating tuna_test.record..."
#python generate_tfrecord.py \
#    --csv_input /media/thk/workspace/DataSet/tuna/tuna_detect_proj/train_eval/tuna_test.csv \
#    --output_path /media/thk/workspace/DataSet/tuna/tuna_detect_proj/train_eval/tuna_test.record \
#    --labels_file /media/thk/workspace/DataSet/tuna/tuna_detect_proj/labels
#    #--image_dir /media/thk/workspace/DataSet/tuna/tuna_detect_proj/images/ \
#
#echo "generating tuna_train.record..."
#python generate_tfrecord.py \
#    --csv_input /media/thk/workspace/DataSet/tuna/tuna_detect_proj/train_eval/tuna_train.csv \
#    --output_path /media/thk/workspace/DataSet/tuna/tuna_detect_proj/train_eval/tuna_train.record \
#    --labels_file /media/thk/workspace/DataSet/tuna/tuna_detect_proj/labels
#    #--image_dir /media/thk/workspace/DataSet/tuna/tuna_detect_proj/images/ \

#echo "generating clownfish_test.record..."
#python generate_tfrecord.py \
#    --csv_input /media/thk/workspace/DataSet/clownfish/clownfish_detect_proj/train_eval/clownfish_test.csv \
#    --output_path /media/thk/workspace/DataSet/clownfish/clownfish_detect_proj/train_eval/clownfish_test.record \
#    --labels_file /media/thk/workspace/DataSet/clownfish/clownfish_detect_proj/labels
#
#echo "generating clownfish_train.record..."
#python generate_tfrecord.py \
#    --csv_input /media/thk/workspace/DataSet/clownfish/clownfish_detect_proj/train_eval/clownfish_train.csv \
#    --output_path /media/thk/workspace/DataSet/clownfish/clownfish_detect_proj/train_eval/clownfish_train.record \
#    --labels_file /media/thk/workspace/DataSet/clownfish/clownfish_detect_proj/labels

