#https://github.com/opencv/opencv/wiki/TensorFlow-Object-Detection-API
import cv2
import time
import numpy as np

colors = [
        (255,255,255),
        (255,0,0),
        (0,255,255),
        ]
test_vid_list = [
        '/media/thk/workspace/DataSet/tuna/repo/vids/tuna-dzpyA-NlRMU_clip.mp4',
        '/media/thk/workspace/DataSet/tuna/repo/vids/tuna-KTOQD4bfKLM_clip.mp4',
        '/media/thk/workspace/DataSet/tuna/repo/vids/tuna-y-Wj685wPIY_clip.mp4',
        ]
#'/home/thk/Downloads/soccer-ball.mp4'
src = 0 #test_vid_list[2] 

MODEL = '/media/thk/workspace/gitlab_proj/tf_model_train/trained/20200407/tuna_ssd_incep_v2_200k_frozen/frozen_inference_graph.pb'
CONFIG = '/media/thk/workspace/gitlab_proj/tf_model_train/trained/20200407/tuna_ssd_incep_v2_200k_frozen/frozen_inference_graph.pbtxt'
LABEL_FILE = '/media/thk/workspace/DataSet/tuna/tuna_detect_proj/labels'
NET = cv2.dnn.readNetFromTensorflow(MODEL, CONFIG)
try:
  with open(LABEL_FILE) as f:
    LABELS = [line.rstrip() for line in f]
except:
  LABELS = None


def draw_results(img, objects, infer_time=None):
  height, width, _ = img.shape
  for obj in objects[0,0,:,:]:
    score = float(obj[2])
    if score > 0.9:
      idx = int(obj[1])-1 # NOTE: use -1 to shift the index for 
                          # model trained without bg label
      xmin = int(obj[3] * width)
      ymin = int(obj[4] * height)
      xmax = int(obj[5] * width)
      ymax = int(obj[6] * height)
      cv2.rectangle(img, (xmin,ymin), (xmax,ymax), colors[idx], 2)
      if LABELS:
        label = LABELS[idx] 
        text_w = 110
      else:
        label = int(obj[1])
        text_w = 60
      cv2.rectangle(img, (xmin,ymin), (xmin+text_w,ymin+20), colors[idx], -1)
      det_info = '{}:{:.2f}'.format(label, score)
      cv2.putText(img, det_info, (xmin+5,ymin+15), 1, 1.0, (0,0,0), 1)

  if infer_time:
    #cv2.rectangle(img, (0,0), (100,20), (0,0,0), -1)
    infer_time_info = '{:.2f} FPS'.format(1.0/infer_time)
    cv2.putText(img, infer_time_info, (10,15), 1, 1.0, (255,255,255), 1)

  return img


def main():
  cap = cv2.VideoCapture(src)
  fourcc = cv2.VideoWriter_fourcc(*'MJPG')
  out = cv2.VideoWriter('output-02.avi', 
          fourcc, 24.0, 
          (int(cap.get(3)), int(cap.get(4))))

  while True:
    _, frame = cap.read()
    #frame = cv2.imread('/home/thk/Downloads/143819.jpg')

    # The following code was used to test the CONTIGUOUS
    #print(frame.dtype, frame.flags)
    #frame = np.array(frame[:,:,::-1], dtype=np.uint8)
    #frame = frame[:,:,::-1]
    #print(frame.dtype, frame.flags)
  
    infer_start = time.perf_counter()    
    NET.setInput(cv2.dnn.blobFromImage(frame, size=(300, 300), swapRB=True, crop=False))
    objects = NET.forward()
    infer_time = time.perf_counter() - infer_start

    res = draw_results(frame, objects, infer_time=None)
    #out.write(res)

    cv2.imshow('res', res)
    if cv2.waitKey(1) & 0xFF == ord('q'):
      break

  cap.release()
  out.release()
  cv2.destroyAllWindows()


if __name__=='__main__':
  main()
