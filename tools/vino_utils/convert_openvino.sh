#python3 /opt/intel/openvino_2019.1.133/deployment_tools/model_optimizer/mo_tf.py \
#    --input_model trained_190622/tf_1_12_frozen_models/frozen_inference_graph.pb \
#    --tensorflow_object_detection_api_pipeline_config ssd_mobilenet_v2_coco_2018_03_29/pipeline.config \
#    --tensorflow_use_custom_operations_config /opt/intel/openvino_2019.1.133/deployment_tools/model_optimizer/extensions/front/tf/ssd_v2_support.json \
#python3 /opt/intel/openvino_2019.3.376/deployment_tools/model_optimizer/mo_tf.py \

python3 /opt/intel/openvino_2020.2.120/deployment_tools/model_optimizer/mo_tf.py \
    --input_model ../../trained/20200413/tuna_ssd_mobilenet_v2_300k_frozen/frozen_inference_graph.pb \
    --tensorflow_object_detection_api_pipeline_config ../../trained/20200413/tuna_ssd_mobilenet_v2_300k_frozen/pipeline.config \
    --transformations_config ./ssd_support_api_v1.14.json \
    --output=detection_boxes,detection_scores,num_detections \
    --data_type FP16 \
    --reverse_input_channels \
    --model_name ssd_mobilenet_v2_coco_tuna_20200413_IR_V7 \
    --output_dir ../../trained/20200413/ir_models/ \
    --generate_deprecated_IR_V7
    #--log_level=DEBUG \
    #--input_shape [1,300,300,3] \

    #--input_meta_graph trained_190622/model.ckpt-300000.meta \
    #--output_dir=/home/thk/openvino_models/ir/FP16/object_detection/ssd_mobilenet_v2_coco/
    #--tensorflow_object_detection_api_pipeline_config ~/openvino_models/object_detection/common/ssd_mobilenet_v2_coco/tf/ssd_mobilenet_v2_coco.config \

# [2020-04-14]
# NOTE:
#    --tensorflow_use_custom_operations_config
#    has been renamed to
#    --transformations_config
#
# Use pipeline.config in the frozen model folder for --tensorflow_object_detection_api_pipeline_config 
