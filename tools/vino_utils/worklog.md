[2020-04-14]
* `ssd_*_support*.json` files can be found in:
  `/opt/intel/openvino_2020.2.120/deployment_tools/model_optimizer/extensions/front/tf`
* `--tensorflow_use_custom_operations_config`
  has been renamed to
  `--transformations_config`.
* Use `pipeline.config` generated in the frozen model folder for 
  `--tensorflow_object_detection_api_pipeline_config`.

[2020-04-15]

* The latest toolkit of Raspbian is `l_openvino_toolkit_runtime_raspbian_p_2020.1.023.tgz`.
  The model should be optimiaed using `--generate_deprecated_IR_V7` to be used with
  the toolkit.

* The toolkit has no `read_network` function, so we should use the following old way:
  ```
  net = IENetwork(model=model_xml, weights=model_bin)
  ```
  instead of the newer API:
  ```
  net = ie.read_network(model=model_xml, weights=model_bin)
  ```

