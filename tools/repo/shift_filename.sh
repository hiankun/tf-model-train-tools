# https://thepcspy.com/read/bulk-rename-files-in-ubuntu/
# https://askubuntu.com/a/478197
# https://stackoverflow.com/a/4561801/9721896
# usage: bash shift_filename.sh path start
path=$1
start=$2
export start
rename -n 's/\d+/our $i; sprintf("%03d", $ENV{start}+$i++)/e' ${path}*.jpeg
