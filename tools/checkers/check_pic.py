from PIL import Image
import numpy as np
from pathlib import Path
from tqdm import tqdm
import argparse


parser = argparse.ArgumentParser(description='Check the downloaded images.')
parser.add_argument('-i', '--img_dir', 
    required=True, type=Path,
    help='directory to the images')
parser.add_argument('-o', '--bad_log', 
  default='./bad_img_list',
  help='the output file for problematic images list')

args = parser.parse_args()
bad_img_list = args.bad_log
num_files = sum(1 for _ in Path(args.img_dir).rglob('*.*'))
n_bad = 0

with open(bad_img_list, 'w') as f:
  for img_path in tqdm(Path(args.img_dir).rglob('*.*'), total=num_files):
    try: 
      img = Image.open(img_path) 
    except IOError: 
      n_bad += 1
      f.write('{}\n'.format(img_path))
    try: 
      img= np.array(img, dtype=np.float32) 
    except : 
      n_bad += 1
      f.write('{}\n'.format(img_path))
    #print('{} checked...'.format(img_path))

if n_bad == 0:
  print('No bad image found.')
else:
  print('\033[91m\033[1mTotally {} bad files detected.\n\033[93mCheck {}.\033[0m'
    .format(n_bad, bad_img_list))
