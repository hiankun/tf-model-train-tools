# find files larger than 5 MB (+5M) and list them
# bash find_big_files.sh path_to_find size_threshold (+-M)

usage="$(basename "$0") [-h] [<-i|-t> dir_to_images +xM] -- find images larger than x MB and shrink it in half

where:
-h  show this help text
-i  set the image directory
-t  set the image directory (test run; won't shrink the found images)

\e[1;33mNOTE: Make sure to convert png to jpeg first, or the shrinking might be useless.
You may use 'mogrify -format jpeg *.png' to convert the png images in place.\e[0m"


while getopts ':hi:t:' option; do
  case "$option" in
    h) echo -e "$usage"
      exit
      ;;
    i) find $2 -size $3 2>/dev/null -exec du -h {} \; -exec mogrify -quality 50% {} \;
      ;;
    t) find $2 -size $3 -exec du -h {} \; #-printf "%p [%k kB]\n"
      ;;
    :) printf "\n\e[1;31m[Err] missing argument for -%s\e[0m\n\n" "$OPTARG" >&2
      echo -e "$usage" >&2
      exit 1
      ;;
    \?) printf "\n\e[1;31m[Err] illegal option: -%s\e[0m\n\n" "$OPTARG" >&2
      echo -e "$usage" >&2
      exit 1
      ;;
  esac
done
