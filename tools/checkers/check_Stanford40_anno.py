# check the bounding boxes in the Stanford40 dataset
import cv2
import argparse
from pathlib import Path
import xml.etree.ElementTree as ET

def get_bboxes(img_path, anno_dir):
  bboxes = []

  anno_path = anno_dir.joinpath(img_path.stem + '.xml')
  tree = ET.parse(anno_path)
  root = tree.getroot()

  img_file = root.findtext('filename')
  print(img_file)

  for bndbox in root.iter('bndbox'):
    xmax = int(bndbox.find('xmax').text)
    xmin = int(bndbox.find('xmin').text)
    ymax = int(bndbox.find('ymax').text)
    ymin = int(bndbox.find('ymin').text)
    bboxes.append([xmax, xmin, ymax, ymin])

  return bboxes


def draw_bboxes(img, bboxes):
  for bbox in bboxes:
    xmax, xmin, ymax, ymin = bbox
    cv2.rectangle(img, (xmin,ymin), (xmax,ymax), (0,255,255), 2)

  return img


def main():
  parser = argparse.ArgumentParser(description='Check the annotated images.')
  parser.add_argument('-i', '--img_dir', 
      required=True, type=Path,
      help='directory to the images')
  parser.add_argument('-a', '--anno_dir', 
      required=True, type=Path,
      help='directory to the annotations')
  
  args = parser.parse_args()
  #num_files = sum(1 for _ in Path(args.img_dir).rglob('*.*'))
  
  
  for img_path in Path(args.img_dir).rglob('*.*'):
    try: 
      img = cv2.imread(str(img_path))
    except IOError: 
      print('cannot open {}...'.format(img_path))
  
    bboxes = get_bboxes(img_path, args.anno_dir)
    img = draw_bboxes(img, bboxes)
  
    cv2.imshow('view annotations', img)
    if cv2.waitKey(0) & 0xFF == ord('q'):
      break
  

if __name__=='__main__':
  main()
