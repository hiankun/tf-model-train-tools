import os
import tensorflow as tf
from tqdm import tqdm
from glob import glob


photo_filenames = glob('../../twice_pics/*.*')
chunk = 100 # use small chunk to prevent throating the resource
file_lists = [photo_filenames[x:x+chunk] for x in range(0, len(photo_filenames), chunk)]

for i, file_list in enumerate(file_lists):
    with tf.Session() as sess:
        print("session[{}]: ".format(i))
        init = tf.global_variables_initializer()
        sess.run(init)
        for filename in tqdm(file_list):
            filecontents = tf.read_file(filename)
            image = tf.image.decode_jpeg(filecontents, channels = 3)
            #image = tf.Print(image,[filename]) 
            try:
                sess.run(image)
            except Exception as e:
                print(filename)
                print (e)
    tf.reset_default_graph()
