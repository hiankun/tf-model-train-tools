# bash resize_large_pics.sh path/
size=1024
identify -format '%w %h %i\n' ${1}*.jpeg | \
    awk -v size=$size '$1 > size || $2 > size {sub(/^[^ ]* [^ ]* /, ""); print}' | \
    tr '\n' '\0' | \
    xargs -0 mogrify -resize "$size"x"$size" -print "resizeing %f\n"

