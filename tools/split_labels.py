import numpy as np
import pandas as pd
np.random.seed(1)

#input_file = '/media/thk/transcend_1T/DataSet/DetAction2019/fall_aug.csv'
#input_file = '/media/thk/transcend_1T/DataSet/DetAction2019/fall.csv'
#input_file = '/media/thk/workspace/DataSet/tuna/tuna_detect_proj/train_eval/tuna.csv'
#input_file = '/media/thk/transcend_2T/DataSet/AquaData/grouper/training/tf_train_data/20200902/grouper.csv'
#input_file = '/media/thk/transcend_1T/DataSet/pigs/records-20200723/tf_train_data/20200830/pig.csv'
#input_file = '/media/thk/transcend_1T/DataSet/pigs/records-20200723/tf_train_data/20200902/pig.csv'
#input_file = '/media/thk/transcend_1T/DataSet/pigs/records-20200723/tf_train_data/20200913/pig.csv'
input_file = '/media/thk/transcend_2T/DataSet/AquaData/clownfish/training/tf_train_data/20201009/clownfish.csv'

train_csv_file = input_file.split('.')[0] + '_train.csv'
test_csv_file = input_file.split('.')[0] + '_test.csv'

#full_labels = pd.read_csv('../twice_tfrecord.csv')
full_labels = pd.read_csv(input_file)
#print(full_labels.head())

#grouped = full_labels.groupby('filename')
#grouped.apply(lambda x: len(x)).value_counts()

# split each file into a group in a list
print('Grouping filenames...')
gb = full_labels.groupby('filename')
grouped_list = [gb.get_group(x) for x in gb.groups]
num_items = len(grouped_list)

print('Getting indexes...')
train_index = np.random.choice(len(grouped_list), size=int(num_items*0.9), replace=False)
test_index = np.setdiff1d(list(range(num_items)), train_index)

print('Creating lists...')
train = pd.concat([grouped_list[i] for i in train_index])
test = pd.concat([grouped_list[i] for i in test_index])

print('Train items: {}\nTest items: {}'.format(len(train), len(test)))

train.to_csv(train_csv_file, index=None)
test.to_csv(test_csv_file, index=None)

print('{}\nand\n{}\nhas been saved...'.format(train_csv_file, test_csv_file))

