# Rotate images to landscape orientation and then
# resize it to smaller on if its resolution exceeds
# the threshold.

import cv2
import os
import argparse
from glob import iglob
from datetime import datetime

# Note that SIZE_THLD is in the format of (W, H) which is
# different from cv2's format (H, W)
SIZE_THLD = (1920, 1080)
img_ext = ['*.png', '*.jpg', '*.jpeg']

def get_ratio(h, w):
    W, H = SIZE_THLD
    if h > w: h,w = w,h
    h_ratio = H/h
    w_ratio = W/w
    if h_ratio > 1 and w_ratio > 1:
        return None
    #print('<<<', h_ratio, w_ratio)
    return h_ratio if h_ratio < w_ratio else w_ratio

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--folder', required=True,
            help='path to the folder of input images')
    parser.add_argument('-b', '--backup', default=True,
            help='auto save images to backup folder before resizing')
    args = parser.parse_args()

    time_stamp = datetime.now().strftime('%Y%m%d_%H%M%S')
    backup_folder = os.path.join(os.path.abspath(args.folder), 'backup_'+time_stamp)
    if args.backup:
        os.mkdir(backup_folder)
        print('Back up folder {} has been created...'.format(backup_folder))

    num_resized = 0
    for ext in img_ext:
        for f in iglob(os.path.join(args.folder, ext)):
            img = cv2.imread(f)
            h, w = img.shape[:2]
            ratio = get_ratio(h, w)
            if ratio:
                back_filename = os.path.join(backup_folder, os.path.basename(f))
                cv2.imwrite(back_filename, img)
                print('{} has been backed up...'.format(back_filename))
                resized_img = cv2.resize(img, (0,0), fx=ratio, fy=ratio)
                cv2.imwrite(f, resized_img)
                print('{} has been resized to {}x{}...'.format(
                    f, resized_img.shape[1], resized_img.shape[0]))
                num_resized += 1

    if num_resized == 0:
        os.rmdir(backup_folder)
        print('No image has been resized. Backup folder is empty and auto removed.')


if __name__=='__main__':
    main()
